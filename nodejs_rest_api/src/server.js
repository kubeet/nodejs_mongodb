// server.js

// base setup

function responseFormater(success, json, error = undefined){
  json.success = success;

  if(error != undefined)
    json.error = error;

  return json;
}

var mongoose = require('mongoose')
mongoose.connect('mongodb://mongo:27017/devices')

// call the packages


var express = require('express')
var app = express();
var bodyParser = require('body-parser');

// config app to use bodyParser()

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

var port = process.env.PORT || 8081; 

// routes for our api

var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.header('Content-Type', 'application/json; charset=utf-8');
  next();
});

var User = require('./app/models/user');
var Device = require('./app/models/device');
var Sensor = require('./app/models/sensor');

// more routes for our API will happen here
router.route('/devices')
  
  .get(function(req, res){
    Device.find().populate('owner', '-_id -password').exec(
      function(error, value){
        res.json(value);
      });
  })

router.route('/device/:id')
  .get(function(req, res){
    var id = req.params.id;
    if (id == undefined || !id.match(/^[0-9a-fA-F]{24}$/)) {
      res.json(responseFormater(false, {}, "id is invalid"));
    }
    else{
      Device.findOne({_id : mongoose.Types.ObjectId(id)}).exec(function(error, device){
        if(device == null)
          res.json(responseFormater(false, {}, 'invalid device id'));
        else{
            Sensor.distinct("name",{owner: device._id}).exec(function(error, result){
              if(!error)
                res.json(responseFormater(true, result));
              else
                res.json(responseFormater(false, {}, error));
            });
        }
      });
    }
  });

router.route('/device/:id/:name/statistic')
  .get(function(req, res){
    var id = req.params.id;
    var name = req.params.name;

    if (id == undefined || !id.match(/^[0-9a-fA-F]{24}$/)) {
      res.json(responseFormater(false, {}, "id is invalid or name is required"));
    }
    else{
      Device.findOne({_id : mongoose.Types.ObjectId(id)}).exec(function(error, device){
        if(device == null)
          res.json(responseFormater(false, {}, 'invalid device id'));
        else{
            Sensor.count({owner: device._id, name: name}).exec(function(error, count){
              if(!error){
                if(count == 0){
                  res.json(responseFormater(false, {}, "Param not found on device"));
                }
                else{
                  Sensor.aggregate(
                    [
                      { 
                        $match: { name: name, owner: device._id } 
                      },
                      { $sort: { date: -1 } },
                      { 
                        $group: { 
                          min : {$min : "$numericValue"},
                          max : {$max : "$numericValue"},
                          avg : {$avg : "$numericValue"},
                          last : {$last : "$numericValue"},
                          _id : name 
                        } 
                      }
                    ], function(error, values){
                      if(error)
                        res.json(responseFormater(false, {}, error));
                      else
                        res.json(responseFormater(true, values[0]));
                    }
                  )

                }
              }
              else
                res.json(responseFormater(false, {}, error));
            });
        }
      });
    }
  });

router.route('/device/:id/:name/last')
  .get(function(req, res){
    var id = req.params.id;
    var name = req.params.name;

    var quantity = req.query.amount;
    if(quantity == undefined)
      quantity = 20;

    if (id == undefined || !id.match(/^[0-9a-fA-F]{24}$/) || isNaN(quantity) ) {
      res.json(responseFormater(false, {}, "id is invalid or name is required. quantity must be numeric if present"));
    }
    else{
      Device.findOne({_id : mongoose.Types.ObjectId(id)}).exec(function(error, device){
        if(device == null)
          res.json(responseFormater(false, {}, 'invalid device id'));
        else{
            Sensor.find({owner: device._id, name: name}).select('-_id').limit(quantity).exec(function(error, result){
              if(error)
                res.json(responseFormater(false, {}, error));
              else
                res.json(responseFormater(true, result));
            });
        }
      });
    }
  });


// register our routes
app.use('/api', router);

// start the server
app.listen(port);
